## 参考サイト
公式が分量多すぎるため、以下のサイトを参考に変更
[CI/CDしてみる](https://dev.classmethod.jp/articles/gitlab-runner-ci-cd-1/)

### ec2を新規で立ち上げ以下のコマンドを実行
```bash
sudo yum update
sudo yum install -y docker
sudo systemctl enable docker
sudo systemctl start docker
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo /usr/local/bin/gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo systemctl enable gitlab-runner
sudo systemctl start gitlab-runner
```
ここまででgitlab-runnerをインストール・稼働まで完了。

### 対話シェルでgitlab-runnerを設定
```bash
sudo /usr/local/bin/gitlab-runner register
```
これが完了するとGitLabにてSpecific Runners完成。GitLabのページでジョブを実行できる特定のRunnerが表示される

### sumプロジェクトインストールしてこれをpushして自動デプロイ自動テストできるか試してみる
- [sum CLIインストール](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-mac.html)
- .gitlab-ci.yml作成
- 事前にIAMユーザーを作成した上で、GitLabにAWS_ACCESS_KEY_IDとAWS_SECRET_ACCESS_KEYをセットする
- このプロジェクトをpushすると自動デプロイ自動テストが走る